/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enuns;

/**
 *
 * @author jcmar
 */
public enum Status {
    AGARDANDO("Aguardando"),
    EM_ATENDIMENTO("Em atendimento!");
    
    public String estado;

    private Status(String estado) {
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }    
}
