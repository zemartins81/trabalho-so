/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enuns;

/**
 *
 * @author jcmar
 */
public enum Prioridades {
   prioridadeZero("0P"),
   prioridadeUm("1P"),
   prioridadeDois("2P");
   
   public String valor;

    private Prioridades(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
