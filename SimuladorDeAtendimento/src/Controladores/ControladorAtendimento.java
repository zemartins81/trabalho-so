/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import elementos.Guiche;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author jcmar
 * @param <E>
 */
public class ControladorAtendimento {
    
    private static ControladorAtendimento instance;
    private final int qtdGuiches;
    private int contador;
    private final List<Guiche> guiches;

    private ControladorAtendimento() {
        this.qtdGuiches = geraQtdGuiches();
        this.contador = 0;
        this.guiches = new ArrayList<>();
        geraGuiches(qtdGuiches);
    }
    
    public ControladorAtendimento getInstance(){
        if(instance == null){
            return instance = new ControladorAtendimento();
        }
        return instance;
    }    
    
    private int geraQtdGuiches(){
        Random ran = new Random();
        return ran.nextInt(11);
    }
    
    private void geraGuiches(int qtd){
        for(int i = 1; i <= qtd; i++){
            this.guiches.add(new Guiche("Guiche " + Integer.toString(i)));
        }
        
    }
            
    
    
    
    
}
