/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import elementos.Cliente;
import enuns.Prioridades;
import java.util.LinkedList;

/**
 *
 * @author jcmar
 */
public final class ControladorFilas {
    
    private static ControladorFilas instance;
    private final LinkedList<LinkedList<Cliente>> pessoasParaAtender;
    private int contadorDeAtendidos;
    private int qtdAtender;
    private int contadorPrioridadeZero;
    private int contadorPrioridadeUm;
    private int contadorPrioridadeDois;

    private ControladorFilas() {
        pessoasParaAtender = new LinkedList<>();
        contadorDeAtendidos = 0;
        qtdAtender = 0;
        contadorPrioridadeZero = 0;
        contadorPrioridadeUm = 0;
        contadorPrioridadeDois = 0;
        this.geraFilas();
    }

    public LinkedList<LinkedList<Cliente>> getPessoasParaAtender() {
        return pessoasParaAtender;
    }

    public int getContadorGeral() {
        return contadorDeAtendidos;
    }
    
    
    public int getQtdAtender() {
        return qtdAtender;
    }
    
    public static ControladorFilas getInstance(){
        if(instance == null){
            return instance = new ControladorFilas();
        }
        return instance;
    }

    private void geraFilas(){    
        LinkedList<Cliente> prioridadeZero = new LinkedList<>();
        pessoasParaAtender.add(prioridadeZero);
        LinkedList<Cliente> prioridadeUm = new LinkedList<>();
        pessoasParaAtender.addLast(prioridadeUm);
        LinkedList<Cliente> prioridadeDois = new LinkedList<>();
        pessoasParaAtender.addLast(prioridadeDois);    
    }
    
    public synchronized void posicionaNovoCliente(Cliente novo){
        if(novo.getId().equalsIgnoreCase(Prioridades.prioridadeZero.getValor())){
            novo.setOrdem(pessoasParaAtender.get(0).size());
            pessoasParaAtender.get(0).addLast(novo);
        }else if(novo.getId().equalsIgnoreCase(Prioridades.prioridadeUm.getValor())){
            novo.setOrdem(pessoasParaAtender.get(1).size());
            pessoasParaAtender.get(1).addLast(novo);
        }else{
            novo.setOrdem(pessoasParaAtender.get(2).size());
            pessoasParaAtender.get(2).addLast(novo);
        }
        qtdAtender++;
    }

    
    private void alteraFila(int entra, int sai){
        pessoasParaAtender.get(entra).addLast(pessoasParaAtender.get(sai).removeFirst());
        pessoasParaAtender.get(sai).removeFirst();
        alteraContador(sai);
    }
    
    public synchronized Cliente poeEmAtendimento(int valor){
        if(valor <= 2){
            if(pessoasParaAtender.get(valor).getFirst() == null){
                poeEmAtendimento(valor++);
            }
            Cliente removeFirst = pessoasParaAtender.get(valor).removeFirst();
            alteraContador(valor);
            return removeFirst;
        }
        return null;
    }   
    
    private void alteraContador(int valor){
        switch(valor){
            case 0:
                contadorPrioridadeZero++;
                avaliaPosicao(contadorPrioridadeZero, 0);
                break;
            case 1:
                contadorPrioridadeUm++;
                avaliaPosicao(contadorPrioridadeUm, 1);
                break;
            case 2:
                contadorPrioridadeDois++;
                break;
            default:
        }
        contadorDeAtendidos++;
    }
    
    private void avaliaPosicao(int contador, int numeroFila){
        int filaQueSai = numeroFila + 1;
        if(contador%3 == 0){
            alteraFila(numeroFila, filaQueSai);
        }
    }
}
