/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elementos;

import elementos.Cliente;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jcmar
 */
public class Guiche{
    
    private String nome;
    private Cliente emAtendimento;
    private List<Cliente> pessoasATendidas;
    private Boolean atendendo;

    public Guiche(String nome) {
        this.nome = nome;
        this.pessoasATendidas = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cliente> getPessoasATendidas() {
        return pessoasATendidas;
    }

    public void setPessoasATendidas(List<Cliente> pessoasATendidas) {
        this.pessoasATendidas = pessoasATendidas;
    }

    public Cliente getEmAtendimento() {
        return emAtendimento;
    }

    public void setEmAtendimento(Cliente emAtendimento) {
        this.emAtendimento = emAtendimento;
    }

    public Boolean getAtendendo() {
        return atendendo;
    }

    public void setAtendendo(Boolean atendendo) {
        this.atendendo = atendendo;
    }
    
    
    
    
    
    
}
