/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elementos;

import enuns.Prioridades;
import Controladores.ControladorFilas;
import enuns.Status;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jcmar
 */
public class GeradorDeClientes extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            Random timer = new Random();
            int tempo = timer.nextInt(11) * 1000;
            if (ControladorFilas.getInstance().getQtdAtender() <= 100) {
                try {
                    Thread.sleep(tempo);
                } catch (InterruptedException ex) {
                    Logger.getLogger(GeradorDeClientes.class.getName()).log(Level.SEVERE, null, ex);
                }
                Random prioRandom = new Random();
                int prioridade = prioRandom.nextInt(3);
                ControladorFilas.getInstance().posicionaNovoCliente(geraCliente(prioridade));
            }
        }

    }

    private Cliente geraCliente(int prioridade) {
        Cliente clienteNovo;
        switch (prioridade) {
            case 0: {
                clienteNovo = new Cliente(Prioridades.prioridadeZero.getValor(), 0, Status.AGARDANDO.getEstado());
                break;
            }
            case 1: {
                clienteNovo = new Cliente(Prioridades.prioridadeUm.getValor(), 0, Status.AGARDANDO.getEstado());
                break;
            }
            default: {
                clienteNovo = new Cliente(Prioridades.prioridadeDois.getValor(), 0, Status.AGARDANDO.getEstado());
                break;
            }
        }
        return clienteNovo;
    }

}
