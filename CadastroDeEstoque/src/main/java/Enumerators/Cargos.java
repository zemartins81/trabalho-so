/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Enumerators;

/**
 *
 * @author jcmar
 */
public enum Cargos {

    ADMINISTRADOR ("Adminstrador"),
    GERENTE ("Gerente"),
    VENDEDOR ("Vendedor"),
    CAIXA ("Caixa");
    
    public String valor;

    private Cargos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
    
    
}
