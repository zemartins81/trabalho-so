/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import Interfaces.IUsuario;
import static javafx.beans.binding.Bindings.and;

/**
 *
 * @author jcmar
 */
public class Funcionario extends Usuario implements IUsuario {

    private String usuario;
    private String senha;
    private String cargo;
    private double salario;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Override
    public boolean login(String usuario, String senha) {
        if ((usuario == null ? this.usuario == null : usuario.equals(this.usuario)) && (senha == null ? this.senha == null : senha.equals(this.senha))){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }
    }

}
